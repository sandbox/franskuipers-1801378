class Vm                                 # default virtual machine settings
  def self.descendants
    ObjectSpace.each_object(::Class).select {|klass| klass < self }
  end

  Project    = "vm2"
  Shortname  = "drupal7"                 # Vagrant name
  Longname   = "Drupal 7 latest version" # VirtualBox name
  Subnet     = "11"                      # 192.168.###.0/24 subnet for this network
  Ansible_tags = "common,drush,drupal-dl,debug"

  Count     = 1                          # number of VMs to create
  Basebox    = "precise32"
  Box_url    = "http://files.vagrantup.com/precise32.box"
  NFS_shares = { "www" => "/var/www", }
  Memory    = 512                        # default VM memory
  Domain    = "local"                    # default domain
  Gui       = false                      # start VM with GUI?
  Verbose   = false                      # make output verbose?
  Debug     = false                      # output debug info?
  SSH_tries = 5                          # How quickly to fail should Vagrant hang
  SSH_forward_agent = false              # Whether to forward SSH agent
  Network   = "192.168"                  # Private network address: ###.###.0.0
  Host_IP   = "10"                       # Starting host address: 192.168.0.###
  SSH_range = (32200..32250)
end

